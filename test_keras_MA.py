import os
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM,Dense
from tensorflow.keras.layers import Embedding, Dropout
import pandas as pd

#Nachdem das Programm definiert hat, welche Argumente es benötigt, argparse findet heraus, wie es diese aus sys.argv auslesen kann
import argparse


def parse_arg():
    parser = argparse.ArgumentParser()

    # hyperparameters, die später eingestellt werden müssen, werden hier als command-line arguments addiert
    parser.add_argument('--epochs', type=int, default=60)
    parser.add_argument('--batch-size', type=int, default=50)
    parser.add_argument('--learning-rate', type=float, default=0.1)
    

    parser.add_argument('--gpu-count', type=int, default=os.environ['SM_NUM_GPUS'])

    # input data und model Ordner
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--train', type=str, default=os.environ['SM_CHANNEL_TRAIN'])
    parser.add_argument('--test', type=str, default=os.environ['SM_CHANNEL_VALIDATION'])
    parser.add_argument('--validation', type=str, default=os.environ['SM_CHANNEL_TEST'])
    
    #Host (local or Sagemaker instance)
    parser.add_argument('--host', type = str, default = json.loads('SM_HOST'))
    parser.add_argument('--current-host', type = str, default = os.environ['SM_CURRENT_HOST'])

    args, _ = parser.parse_known_args()
    
    return args


#convert train, val- and test data into inputs and aoutputs sequences
def to_sequences(_data, n_input, n_out = t_step):
    X, y = list(), list()
    in_start = 0
    
     
    
    # step over the entire history one time step at a time
    for _ in range(len(train_data)):
        #define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_out
        
        #import pdb; pdb.set_trace()
        
        # ensure we have enough data for this instance
        if out_end <= len(train_data):
            X.append(train_data[in_start:in_end, :])
            y.append(train_data[in_end:out_end, index_leader_Speed])
        # move along one time step
        in_start += 1
        
    x_seq= np.array(X)
    y_seq= np.array(y)
       
    return x_seq, y_seq


# split a multivariate dataset into train/test sets for Training and Test
def split_dataset(data):
    train_data, val_data, test_data = data[:train_split], data[train_split-1:len(data)-val_split], data[len(data)-val_split-1:]
    return train_data, val_data, test_data 




""" Building the model"""
def build_model( X_Train, Y_Train, X_Val, Y_Val, n_input):
    
    # define parameters
    verbose, = 1
    #epochs, batch_size = 60, 50
    
    
    n_timesteps, n_features, n_outputs = X_Train.shape[1], X_Train.shape[2], Y_Train.shape[1]
    # reshape output into [samples, timesteps, features]
    Y_Train = Y_Train.reshape((Y_Train.shape[0], Y_Train.shape[1], 1))
    
    #define model
    
    #ENCODER
    model = Sequential()
    model.add(LSTM(288, activation='relu', input_shape=(n_timesteps, n_features)))
    model.add(Dropout(0.2))
    
    model.add(RepeatVector(n_outputs))
    
    #DECODER
    model.add(LSTM(288, activation='relu', return_sequences=True))
    model.add(TimeDistributed(Dense(288, activation='relu')))
    model.add(TimeDistributed(Dense(1)))
    model.compile(loss='mae', optimizer='adam', metrics=['acc', 'mae'])
    
    # fit network
    history = model.fit(X_Train, Y_Train, epochs=epochs, batch_size=batch_size, validation_data=(X_Val, Y_Val),  verbose=verbose)
    
    # Model Architectur
    print(model.summary())
    acc = history.history['acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1, len(acc) + 1)
    
    #plotting training and validation loss
    plt.plot(epochs, loss, label='Training loss')
    plt.plot(epochs, val_loss, label='validation loss')
    plt.title('Training and validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()
    
    #plotting training and validation accuracy
    plt.clf()

    val_acc = history.history['val_acc']
    plt.plot(epochs, acc, label='Training acc')
    plt.plot(epochs, val_acc, label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('acc')
    plt.legend()
    plt.show()
    
    #model.save('LSTM_Model_Multistage08_8s_CT_4_24.h5')
    return model

def load_data(file_path, channel):
    #hole ein Paket von Datein aus einem gegebenen Ordner und lis sie alle in einen einzelnen pandas Dataframe
    input_files = [os.path.join(file_path, file) for file in os.listdir(file_path)]
    if len(input_files) == 0:
        raise ValueError(("There are no files in {}.\n" +
                          "This usually indicates that the channel ({}) was uncorrectly specified, \n" +
                          "the data specification in s3 was incorrectly specified or the role specified\n" +
                          "does not have permission to acces the data.").format(file_path, channel))
    
    raw_data = [pd.read_csv(file, header = None, engine = "python") for file in input_files]
    df = pd.concat(raw_data)
    
    features = df.iloc[:,1:].values
    label = df.iloc[:,0].values
    
    return features, label


def _parser_args():
    
    parser = argparse.ArgumentParser()
    
    
    #Hyperparameters
    parser.add_argument('--epochs', type = int, default =60)
    parser.add_argument('--batch-size', type = int, default =50)
    
    
    #Ordner für Data, model an output
    parser.add_argument('--model_dir', type = str)
    parser.add_argument('--sm_model_dir', type = str, default = os.environ.get('SM_MODEL_DIR'))
    parser.add_argument('--train', type = str, default = os.environ.get('SM_CHANNEL_TRAINING'))
    parser.add_argument('--test', type = str, default = os.environ.get('SM_CHANNEL_TESTING'))
    parser.add_argument('--validation', type = str, default = os.environ.get('SM_CHANNEL_VALIDATION'))
    
    #Host (local or Sagemaker instance)
    parser.add_argument('--host', type = str, default = json.loads('SM_HOST'))
    parser.add_argument('--current-host', type = str, default = os.environ.get('SM_CURRENT_HOST'))
    
    args, _ = parser.parse_known_args()
    
    return args








if __name__ = "__main__":
    
    #Konstanten
    index_leader_Speed = 8
    n_input = 10
    t_step = 10
    
    args = _parser_args()
    epochs     = args.epochs
    lr         = args.learning_rate
    batch_size = args.batch_size
    gpu_count  = args.gpu_count
    model_dir  = args.model_dir
    training_dir   = args.train
    
    training_data = pd.read_csv('train.csv',sep=',')
    validation_data = pd.read_csv('train.csv',sep=',')
    test_data = pd.read_csv('train.csv',sep=',')
    
    x_train, y_train = to_sequences(train_data, n_input, n_out = t_step)
    x_val, y_val = to_sequences(val_data, n_input, n_out = t_step)
    x_train, y_train = to_sequences(train_data, n_input, n_out = t_step)
     
    
    train_data, train_labels = load_data(args.train, 'train')
    test_data, test_labels = load_data(args.train, 'test')
    validation_data, validation_labels = load_data(args.validation, 'validation')
    
    myModel = build_model(train_data, train_labels, validation_data, validation_labels, n_input)
    
    if args.current_host == args.hosts[0]:
        
        # Model mit der Versionnummer '00000001' speichern
        myModel.save(os.path.join(args.sm_model_dir, '00000001'), 'model_ma.h5')
    
      
    
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          