
import os
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM,Dense, RepeatVector, TimeDistributed
from tensorflow.keras.layers import Embedding, Dropout
import pandas as pd
import numpy as np
import  json



#Nachdem das Programm definiert hat, welche Argumente es benötigt, argparse findet heraus, wie es diese aus sys.argv auslesen kann
import argparse


if __name__ == "__main__":
    

    parser = argparse.ArgumentParser()

    # hyperparameters, die später eingestellt werden müssen, werden hier als command-line arguments addiert
    parser.add_argument('--epochs', type=int, default=60)
    parser.add_argument('--batch-size', type=int, default=50)
    #parser.add_argument('--learning-rate', type=float, default=0.1)
    

    parser.add_argument('--gpu-count', type=int, default=os.environ['SM_NUM_GPUS'])
#     parser.add_argument('--gpu-count', type=int, default=0)

    # input data und model Ordner
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
#     parser.add_argument('--model-dir', type=str, default='/tmp')
    parser.add_argument('--training', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    parser.add_argument('--validation', type=str, default=os.environ['SM_CHANNEL_VALIDATION'])
    #parser.add_argument('--test', type=str, default=os.environ['SM_CHANNEL_TEST'])
    
#     parser.add_argument('--training', type=str, default='data')
#     parser.add_argument('--test', type=str, default='data')
#     parser.add_argument('--validation', type=str, default='data')
    
    args, _ = parser.parse_known_args()
    
    
    epochs     = args.epochs
    #lr         = args.learning_rate
    batch_size = args.batch_size
    gpu_count  = args.gpu_count
    model_dir  = args.model_dir
    training_dir   = args.training
    validation_dir   = args.validation
    
    x_train =np.load(os.path.join(training_dir, 'training.npz'))['image']
    y_train =np.load(os.path.join(training_dir, 'training.npz'))['label']
    x_val =np.load(os.path.join(validation_dir, 'validation.npz'))['image']
    y_val =np.load(os.path.join(validation_dir, 'validation.npz'))['label']
    
    print ("x_train shape:", x_train.shape)
    print ("x_val shape:", x_val.shape)
    print()
    print ("y_train shape:", y_train.shape)
    print ("y_val shape:", y_val.shape)

    
    print(x_val[0,0])
     
    
    n_timesteps, n_features, n_outputs = x_train.shape[1], x_train.shape[2], y_train.shape[1]
    
    # reshape output into [samples, timesteps, features]
    y_train = y_train.reshape((y_train.shape[0], y_train.shape[1], 1))
    y_val = y_val.reshape((y_val.shape[0], y_val.shape[1], 1))
    
    #define model
    
    #ENCODER
    model = Sequential()
    model.add(LSTM(288, activation='relu', input_shape=(n_timesteps, n_features)))
    model.add(Dropout(0.2))
    
    model.add(RepeatVector(n_outputs))
    
    #DECODER
    model.add(LSTM(288, activation='relu', return_sequences=True))
    model.add(TimeDistributed(Dense(288, activation='relu')))
    model.add(TimeDistributed(Dense(1)))
    model.compile(loss='mae', optimizer='adam', metrics=['acc', 'mae'])
    
    # fit network
    history = model.fit(x_train, y_train, epochs=2, batch_size=50, validation_data=(x_val, y_val),  verbose=1)
    
    # Model Architectur
    print(model.summary())
    acc = history.history['acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1, len(acc) + 1)
    
    #plotting training and validation loss
    plt.plot(epochs, loss, label='Training loss')
    plt.plot(epochs, val_loss, label='validation loss')
    plt.title('Training and validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()
    
    #plotting training and validation accuracy
    plt.clf()

    val_acc = history.history['val_acc']
    plt.plot(epochs, acc, label='Training acc')
    plt.plot(epochs, val_acc, label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('acc')
    plt.legend()
    plt.show()
    
    tf.saved_model.simple_save(
        tf.keras.backend.get_session(),
        os.path.join(model_dir, '1'),
        inputs={'inputs': model.input},
        outputs={t.name: t for t in model.outputs})
