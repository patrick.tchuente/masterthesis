# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 13:41:00 2019

@author: Tchuente
"""

import pickle
from data_prepare import*
import pandas as pd


"""Ego Fahrzeug"""

with open('info_egoVeh_myEgoCar1.pickle', 'rb') as handle:
    info_EGO1 = pickle.load(handle)

with open('info_egoVeh_myEgoCar2.pickle', 'rb') as handle:
    info_EGO2 = pickle.load(handle)

with open('info_egoVeh_myEgoCar3.pickle', 'rb') as handle:
    info_EGO3 = pickle.load(handle)
        
with open('info_egoVeh_myEgoCar4.pickle', 'rb') as handle:
    info_EGO4 = pickle.load(handle)
    
with open('info_egoVeh_myEgoCar5.pickle', 'rb') as handle:
    info_EGO5 = pickle.load(handle)
    
with open('info_egoVeh_myEgoCar6.pickle', 'rb') as handle:
    info_EGO6 = pickle.load(handle)
    
# with open('info_egoVeh_myEgoCar7.pickle', 'rb') as handle:
#     info_EGO7 = pickle.load(handle)

# with open('info_egoVeh_myEgoCar8.pickle', 'rb') as handle:
#     info_EGO8 = pickle.load(handle)
   
    
    
""" Leader Fahrzeug"""
with open('info_LEADER_myEgoCar1.pickle', 'rb') as handle:
    info_LEADER1 = pickle.load(handle)
    
with open('info_LEADER_myEgoCar2.pickle', 'rb') as handle:
    info_LEADER2 = pickle.load(handle)
    
with open('info_LEADER_myEgoCar3.pickle', 'rb') as handle:
    info_LEADER3 = pickle.load(handle)
    
with open('info_LEADER_myEgoCar4.pickle', 'rb') as handle:
    info_LEADER4 = pickle.load(handle)
    
with open('info_LEADER_myEgoCar5.pickle', 'rb') as handle:
    info_LEADER5 = pickle.load(handle)

with open('info_LEADER_myEgoCar6.pickle', 'rb') as handle:
    info_LEADER6 = pickle.load(handle)
    
# with open('info_LEADER_myEgoCar7.pickle', 'rb') as handle:
#     info_LEADER7 = pickle.load(handle)
# with open('info_LEADER_myEgoCar8.pickle', 'rb') as handle:
#     info_LEADER8 = pickle.load(handle) 
    
   
    
""" Alle Information aus dem ContextSupcription ( Alle Straßen Teilnehmern aus einem Radius vom Ego Fahrzeug)"""

with open('infoSubcription_myEgoCar1.pickle', 'rb') as handle:
    infoSubcriptionResults1 = pickle.load(handle)
    
with open('infoSubcription_myEgoCar2.pickle', 'rb') as handle:
    infoSubcriptionResults2 = pickle.load(handle)
    
with open('infoSubcription_myEgoCar3.pickle', 'rb') as handle:
    infoSubcriptionResults3 = pickle.load(handle)
    
with open('infoSubcription_myEgoCar4.pickle', 'rb') as handle:
    infoSubcriptionResults4 = pickle.load(handle)
    
with open('infoSubcription_myEgoCar5.pickle', 'rb') as handle:
    infoSubcriptionResults5 = pickle.load(handle)
    
with open('infoSubcription_myEgoCar6.pickle', 'rb') as handle:
    infoSubcriptionResults6 = pickle.load(handle)
    
# with open('infoSubcription_myEgoCar7.pickle', 'rb') as handle:
#     infoSubcriptionResults7 = pickle.load(handle)
    
# with open('infoSubcription_myEgoCar8.pickle', 'rb') as handle:
#     infoSubcriptionResults8 = pickle.load(handle)
    
    
data_set = pd.DataFrame()   


data_set["Ego_Speed"] = info_EGO6['speed']
data_set["Ego_acceleration"] = info_EGO6['acceleration']
data_set["Ego_allowedSpeed"] = info_EGO6['allowedSpeed']
data_set["distEgoToLeader"] = info_LEADER6['distToEgo']

"""  vierter zustand für Ampelanlage"""
data_set["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO6['nextTLS']] 
data_set["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO6['nextTLS']]
cat = pd.factorize(data_set["Ego_SignalTLS"])
data_set["Ego_SignalTLS"] = cat[0]
data_set["meanSpeedOnEgoLane"] = infoSubcriptionResults6['meanSpeedOnEgoLane']
data_set["traffic_flow"] = infoSubcriptionResults6['traffic_fow']
data_set["traffic_density"] = infoSubcriptionResults6['traffic_Density']
data_set["numberOfLane"] = infoSubcriptionResults6['numberLane']
data_set["leader_Speed"] = info_LEADER6['speed']
data_set["leader_acceleration"] = info_LEADER6['acceleration']
data_set["meanSpeedOnEdge"] = infoSubcriptionResults6['meanSpeedOnEdge']
data_set["switch_state_time"] = info_EGO6['time_state_switch']

data_set.to_csv('data_set12_08_2303_CT.csv', index=False)



























































#data_set1["Ego_Speed"] = info_EGO1['speed']
#data_set1["Ego_acceleration"] = info_EGO1['acceleration']
#data_set1["Ego_allowedSpeed"] = info_EGO1['allowedSpeed']
#data_set1["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO1['nextTLS']] 
#data_set1["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO1['nextTLS']]
#cat = pd.factorize(data_set1["Ego_SignalTLS"])
#data_set1["Ego_SignalTLS"] = cat[0]
#
#data_set2["Ego_Speed"] = info_EGO2['speed']
#data_set2["Ego_acceleration"] = info_EGO2['acceleration']
#data_set2["Ego_allowedSpeed"] = info_EGO2['allowedSpeed']
#data_set2["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO2['nextTLS']] 
#data_set2["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO2['nextTLS']]
#cat = pd.factorize(data_set2["Ego_SignalTLS"])
#data_set2["Ego_SignalTLS"] = cat[0]
#
#data_set3["Ego_Speed"] = info_EGO3['speed']
#data_set3["Ego_acceleration"] = info_EGO3['acceleration']
#data_set3["Ego_allowedSpeed"] = info_EGO3['allowedSpeed']
#data_set3["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO3['nextTLS']] 
#data_set3["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO3['nextTLS']]
#cat = pd.factorize(data_set3["Ego_SignalTLS"])
#data_set3["Ego_SignalTLS"] = cat[0]
#
#data_set4["Ego_Speed"] = info_EGO4['speed']
#data_set4["Ego_acceleration"] = info_EGO4['acceleration']
#data_set4["Ego_allowedSpeed"] = info_EGO4['allowedSpeed']
#data_set4["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO4['nextTLS']] 
#data_set4["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO4['nextTLS']]
#cat = pd.factorize(data_set4["Ego_SignalTLS"])
#data_set4["Ego_SignalTLS"] = cat[0]
#
#data_set5["Ego_Speed"] = info_EGO5['speed']
#data_set5["Ego_acceleration"] = info_EGO5['acceleration']
#data_set5["Ego_allowedSpeed"] = info_EGO5['allowedSpeed']
#data_set5["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO5['nextTLS']] 
#data_set5["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO5['nextTLS']]
#cat = pd.factorize(data_set5["Ego_SignalTLS"])
#data_set5["Ego_SignalTLS"] = cat[0]
#
#data_set6["Ego_Speed"] = info_EGO6['speed']
#data_set6["Ego_acceleration"] = info_EGO6['acceleration']
#data_set6["Ego_allowedSpeed"] = info_EGO6['allowedSpeed']
#data_set6["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO6['nextTLS']] 
#data_set6["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO6['nextTLS']]
#cat = pd.factorize(data_set6["Ego_SignalTLS"])
#data_set6["Ego_SignalTLS"] = cat[0]
#
#data_set7["Ego_Speed"] = info_EGO7['speed']
#data_set7["Ego_acceleration"] = info_EGO7['acceleration']
#data_set7["Ego_allowedSpeed"] = info_EGO7['allowedSpeed']
#data_set7["Ego_DistToTLS"] = [ i[0][2] if len(i) != 0 else 500  for i in info_EGO7['nextTLS']] 
#data_set7["Ego_SignalTLS"] = [ i[0][3] if len(i) != 0 else "x" for i in info_EGO7['nextTLS']]
#cat = pd.factorize(data_set7["Ego_SignalTLS"])
#data_set7["Ego_SignalTLS"] = cat[0]
