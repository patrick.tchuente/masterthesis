# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 15:19:15 2020

@author: tchuente
"""


import os
import sys

from collections import namedtuple
from myvehicles import myVehiles, Routes
from data_collector import info_egoVeh, info_LEADER, infoSubcription
#from sty import fg, bg, ef, rs
import pickle
#from termcolor import colored
from TLS_Timing import time_state_switch




#SUMO_HOME = "C:\Program Files (x86)\Eclipse\Sumo"

# import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(
        os.path.dirname(__file__), "..", "..", "..")), "tools"))  # tutorial in docs
except ImportError:
    sys.exit(
        "please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")
    
      
import traci
import traci.constants as tc




def run():
    
    vehID_and_route = dict(zip(my_veh_ID, my_route))
    
    for vehID, rou in vehID_and_route.items():
    
        """Initialise simulation ======================================================================================="""
        
        route = traci.simulation.findRoute(rou[0], rou[1])
        veh_ego = myVehiles(vehID, str("".join(('route_', vehID))), "myEgoCars", route.edges)
        
        traci.route.add(veh_ego.RouteID, route.edges )
        traci.vehicle.add(veh_ego.ID, veh_ego.RouteID, typeID='myEgoCars')  # Ego vehicle
        traci.vehicle.setLaneChangeMode(veh_ego.ID, 257)
        
        # SUMO subscriptions
        traci.vehicle.subscribeLeader(veh_ego.ID, 10000)
        traci.vehicle.subscribe(veh_ego.ID, [tc.VAR_POSITION, tc.VAR_SPEED, tc.VAR_ACCELERATION , tc.VAR_ALLOWED_SPEED, tc.VAR_LANE_INDEX, tc.VAR_NEXT_TLS,
                                             tc.VAR_NEXT_STOPS, tc.VAR_ANGLE, tc.VAR_LANEPOSITION_LAT])
        
        traci.vehicle.subscribeContext(veh_ego.ID, tc.CMD_GET_VEHICLE_VARIABLE, 100.0, [tc.VAR_POSITION, tc.VAR_SPEED, tc.VAR_ACCELERATION, 
                                                                                        tc.VAR_ALLOWED_SPEED, tc.VAR_LANE_INDEX, tc.VAR_LANE_ID, tc.VAR_ANGLE, tc.VAR_LANEPOSITION_LAT])
        traci.vehicle.addSubscriptionFilterLCManeuver( {-1, 1}, noOpposite=False, downstreamDist=None, upstreamDist=None)
        
    #    traci.junction.subscribeContext('gneJ12', tc.CMD_GET_VEHICLE_VARIABLE, 42, [tc.VAR_SPEED, tc.VAR_WAITING_TIME])
    
        
        
                                                     
        time_step = 0
        
        #solange das letzte Ego_vehicle noch unterwegs ist
        while vehID not in traci.simulation.getArrivedIDList():
           traci.simulationStep()
           
           results = traci.vehicle.getAllSubscriptionResults()
           resultContextSub = traci.vehicle.getContextSubscriptionResults(veh_ego.ID)
           
           #wenn das aktuelle Ego_vehice noch unterwegs ist   
           if veh_ego.ID in traci.vehicle.getIDList():
               
               

               print(veh_ego.ID)
               print (time_step)
               
               
               time_step += 1
               
               info_egoVeh['position'].append(results[veh_ego.ID][tc.VAR_POSITION])
               info_egoVeh['lateralLanePosition'].append(results[veh_ego.ID][tc.VAR_LANEPOSITION_LAT])
               info_egoVeh['speed'].append(results[veh_ego.ID][tc.VAR_SPEED])
               info_egoVeh['acceleration'].append(results[veh_ego.ID][tc.VAR_ACCELERATION])
               info_egoVeh['allowedSpeed'].append(results[veh_ego.ID][tc.VAR_ALLOWED_SPEED])
               info_egoVeh['laneIndex'].append(results[veh_ego.ID][tc.VAR_LANE_INDEX])
               info_egoVeh['nextTLS'].append(results[veh_ego.ID][tc.VAR_NEXT_TLS])
               info_egoVeh['nextStop'].append(results[veh_ego.ID][tc.VAR_NEXT_STOPS])
               info_egoVeh['angle'].append(results[veh_ego.ID][tc.VAR_ANGLE])
               info_egoVeh['sim_step'].append(time_step)
               
               
               result_TLS = results[veh_ego.ID][tc.VAR_NEXT_TLS]
               current_simtime  = traci.simulation.getCurrentTime()/1000
               info_egoVeh['time_state_switch'].append(time_state_switch(result_TLS, current_simtime, traci))
               
               
               
               
               """ FAlls das EgoFahrzeug kein Vorderfahrzeug hat"""            
               if results[veh_ego.ID][tc.VAR_LEADER] is None:
                    
                    info_LEADER['speed'].append(results[veh_ego.ID][tc.VAR_SPEED])
                    info_LEADER['acceleration'].append(results[veh_ego.ID][tc.VAR_ACCELERATION])
                    info_LEADER['laneIndex'].append(results[veh_ego.ID][tc.VAR_LANE_INDEX])
                    info_LEADER['distToEgo'].append(1000)
                    info_LEADER['sim_step'].append(time_step)
               else:
                    info_LEADER['speed'].append(traci.vehicle.getSpeed(results[veh_ego.ID][tc.VAR_LEADER][0]))
                    info_LEADER['acceleration'].append(traci.vehicle.getAcceleration(results[veh_ego.ID][tc.VAR_LEADER][0]))
                    info_LEADER['laneIndex'].append(traci.vehicle.getLaneIndex(results[veh_ego.ID][tc.VAR_LEADER][0]))
                    info_LEADER['distToEgo'].append(results[veh_ego.ID][tc.VAR_LEADER][1])
                    info_LEADER['sim_step'].append(time_step)
    
    
               Id, position, speed, acceleration = list(), list(), list(), list()
               allowedSpeed, laneIndex, angle, numberLane  = list(), list(), list(), list()
               speedOfVehOnEgoLane, vehOnEgoLane, edge_speed= list(), list(), list()
 

               #Pro simulations step werden Eigenschaften aller Fahrzeuge sortiert
               for vehiculeID, subc in resultContextSub.items():
                   #hier werden nur Fahrzeuge, die sich auf der Route des Egofahrzeugs befinden, berücksichtigt
                   if traci.vehicle.getRoadID(vehiculeID) in traci.route.getEdges(traci.vehicle.getRouteID(veh_ego.ID)):
                   
                       Id.append(vehiculeID)
                       position.append(resultContextSub[vehiculeID][tc.VAR_POSITION])
                       speed.append(resultContextSub[vehiculeID][tc.VAR_SPEED])
                       edge_speed.append(resultContextSub[vehiculeID][tc.VAR_SPEED])
                       acceleration.append(resultContextSub[vehiculeID][tc.VAR_ACCELERATION])
                       allowedSpeed.append(resultContextSub[vehiculeID][tc.VAR_ALLOWED_SPEED])
                       laneIndex.append(resultContextSub[vehiculeID][tc.VAR_LANE_INDEX])
                       numberLane.append(traci.edge.getLaneNumber(traci.lane.getEdgeID(resultContextSub[vehiculeID][tc.VAR_LANE_ID ])))
                       angle.append(resultContextSub[vehiculeID][tc.VAR_ANGLE])
                   
                       #Nur Fahrzeuge die sich auf der Spur des Egofahrzeug befinden.            
                       if resultContextSub[vehiculeID][tc.VAR_LANE_INDEX ] == results[veh_ego.ID][tc.VAR_LANE_INDEX]:
                           vehOnEgoLane.append(vehiculeID)
                           speedOfVehOnEgoLane.append(resultContextSub[vehiculeID][tc.VAR_SPEED])
                           
                   elif veh_ego.ID not in Id:
                       
                       Id.append(veh_ego.ID)
                       position.append(resultContextSub[veh_ego.ID][tc.VAR_POSITION])
                       speed.append(resultContextSub[veh_ego.ID][tc.VAR_SPEED])
                       edge_speed.append(resultContextSub[veh_ego.ID][tc.VAR_SPEED])
                       acceleration.append(resultContextSub[veh_ego.ID][tc.VAR_ACCELERATION])
                       allowedSpeed.append(resultContextSub[veh_ego.ID][tc.VAR_ALLOWED_SPEED])
                       laneIndex.append(resultContextSub[veh_ego.ID][tc.VAR_LANE_INDEX])
                       numberLane.append(traci.edge.getLaneNumber(traci.lane.getEdgeID(resultContextSub[veh_ego.ID][tc.VAR_LANE_ID ])))
                       angle.append(resultContextSub[veh_ego.ID][tc.VAR_ANGLE])
                       vehOnEgoLane.append(vehiculeID)
                       speedOfVehOnEgoLane.append(resultContextSub[vehiculeID][tc.VAR_SPEED])
                      
                       
               #Mittlere Geschwindigkeit auf Ego-Spur 
               meanSpeedVehOnegoLane = sum(speedOfVehOnEgoLane)/len(speedOfVehOnEgoLane) if len(speedOfVehOnEgoLane) != 0 else  results[veh_ego.ID][tc.VAR_SPEED]
               
               #Anzahl von Fahrspuren auf der aktuellen Edge
               numberOfLane = traci.edge.getLaneNumber(traci.vehicle.getRoadID(veh_ego.ID))
               
               #Verkehrsfluss
               traffic_flow = len(Id)/simulationTimeStep
               
               #Mittlere Geschwindigkeit auf der Edge
               meanSpeedOnEdge = sum(edge_speed)/len(edge_speed)
               
               #Verkehrsdichte
               traffic_density = traffic_flow/meanSpeedOnEdge if meanSpeedOnEdge != 0 else 0
               
                    
               #collector     
               infoSubcription['ID'].append(Id)
               infoSubcription['position'].append(position)
               infoSubcription['speed'].append(speed)
               infoSubcription['acceleration'].append(acceleration)
               infoSubcription['allowedSpeed'].append(allowedSpeed)
               infoSubcription['laneIndex'].append(laneIndex)
               infoSubcription['numberLane'].append(numberOfLane)
               infoSubcription['meanSpeedOnEdge'].append(meanSpeedOnEdge)
               infoSubcription['meanSpeedOnEgoLane'].append(meanSpeedVehOnegoLane)
               infoSubcription['angle'].append(angle)
               infoSubcription['traffic_fow'].append(traffic_flow)
               infoSubcription['traffic_Density'].append(traffic_density)
               
               
               #Gesammelte Information in einer Picke Datei speichern, um sie später anwenden zu können, ohne dass die Simulation neu durchgeführt wird        
               seq_EGO = ('info_egoVeh_',vehID, '.pickle' )
               seq_LEADER = ('info_LEADER_',vehID, '.pickle' )
               seq_INFOSUBCRIPTION = ('infoSubcription_',vehID, '.pickle' )      
                              
               with open(str("".join(seq_EGO)), 'wb') as handle:
                   pickle.dump(info_egoVeh, handle, protocol=pickle.HIGHEST_PROTOCOL)
                    
               with open(str("".join(seq_LEADER)), 'wb') as handle:
                   pickle.dump(info_LEADER, handle, protocol=pickle.HIGHEST_PROTOCOL)
                          
               with open(str("".join(seq_INFOSUBCRIPTION)), 'wb') as handle:
                   pickle.dump(infoSubcription, handle, protocol=pickle.HIGHEST_PROTOCOL)
            
    
    
if __name__ == "__main__":

    """Input ======================================================================================================="""
    my_sumo_config = "osm1DarmstadtCity.sumocfg"
    my_veh_ID = ["myEgoCar1", "myEgoCar2","myEgoCar3", "myEgoCar4", "myEgoCar5", "myEgoCar6", "myEgoCar7", "myEgoCar8", "myEgoCar9"]

    # #route1
    # my_route = [("-259180747", "256810521"), ("4189736#1", "140184337#3"), ("-327100#0", "4276404#0"),("-149706228#5", "177303889#1"), ("4158800#0", "140184337#3"), 
    #               ("331519295", "405088379#2")] # route DarmstadtCity
         
    # #route2   
    # my_route = [("127177148", "-255729743#0"), ("150032814#2", "-534778748#0"), ("53207957", "4158376"),("-405088375#0", "-254355842"), ("249682388", "-150032770#2"), 
    #             ("259176474", "-189656504#0")] # route DarmstadtCity

    # #route3
    # my_route = [("4189736#1", "139668778#1"), ("641143999", "-116794814#7"), ("-252716", "256810522"),("-150041555", "353226875#2"), ("506013711#1", "149706228#4"), 
    #             ("-149706228#4", "140097189#2")] # route DarmstadtCity
    
    
    # #route4              
    # my_route = [("-60263960#2", "150016599#1"), ("-60263960#2", "54899788"), ("4189736#0", "-189656504#0"),("313146734#3", "79723997#0"), ("4075332#2", "60263960#2"), 
    #             ("254290499", "60263960#2")] # route DarmstadtCity   

    # #route5
    # my_route = [("150016567", "150041620#0"), ("405088379#0", "-253549987#1"), ("437418619#0", "149706245#0"),("-405088374#0", "641143998#0"), ("-140186755", "-30751557"), 
    #             ("259179348#3", "4144040#1")] # route DarmstadtCity

    # #route6
    # my_route = [("120484533#3", "-405088379#2"), ("120484533#3", "-405088379#0"), ("-51873805#1", "249682388"),("641143996", "4276404#1"), ("4218533", "-253581531"),
    #             ("-75392300#2","-116794814#5")]
    
    # #route7
    # my_route = [("53094007#5", "60263960#5"), ("53094007#2", "-253117038"), ("116794814#0", "120374292#0"),("-4195580#1", "256810521"), ("-149706228#0", "-4223583"),
    #             ("189656504#1","120374292#0")]
    
    # #route8
    # my_route = [("-149706228#5", "-253581531"), ("-353226875#1", "641143994"), ("-150041555", "61013104"),("127177148", "150041620#0"), ("4216960#1", "60263960#2"),
    #             ("-4075332#0","378479739#0")]
    
    # #route9
    # my_route = [("-294974", "-254355842"), ("313146734#3", "259179350"), ("519448454", "35575436#0"),("-150041555", "150016599#1"), ("53207957", "259179350"),
    #             ("-327099","-196656222")]
    
    # #route10
    # my_route = [("331519295", "51873805#1"),("186513418", "-150023501#2"),("189656504#1", "120374292#0"),("140096358#1", "641143994"),("53094007#4", "256749963"),
    #             ("254290499","259179351#2")]
    
    # #route11
    # my_route = [("--246543","641143997"), ("-150032770#2","53094007#1"), ("-150041579#0","132470959"), ("506013711#1","150016575"), ("-139668778#0","437418618#1"),
    #             ("255729743#1","120374292#1")]
   
    #route12
    my_route = [("-120682496#0","-437418617#6"), ("437418617#0","120374292#0"), ("140358745","61102531#0"), ("-120484533#1","-53094007#3"), ("53094007#3","150032818#1"),
                ("-4218430#0","641143998#0")]
   
          
           


    simulationTimeStep = 0.6
                 


traci.start(["sumo-gui", "-c", my_sumo_config, '--no-warnings'])
run()

     



    


