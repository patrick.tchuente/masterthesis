# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 15:18:17 2020

@author: tchuente
"""

def time_state_switch(result_TLS, current_simtime, traci):
    
    
    TLS_phases = list()
    TLS_programduration = 0
    TLS_redduration = 0
    TLS_greenduration = 0
    TLS_phaseremaindur = 0
    
    
    
    if len(result_TLS) > 0:
        
        TLS_ID = result_TLS[0][0]
        TLS_distance = result_TLS[0][2]
        TLS_state = result_TLS[0][3]
        
        TLS_connID = result_TLS[0][1]
        TLS_time_to_switch = traci.trafficlight.getNextSwitch(TLS_ID) - current_simtime
        TLS_programID = int(traci.trafficlight.getProgram(TLS_ID))
        TLS_program = traci.trafficlight.getCompleteRedYellowGreenDefinition(TLS_ID)[TLS_programID]
        
        
        for i in range(len(TLS_program.phases)):
            TLS_programduration += TLS_program.phases[i].duration
            TLS_phases.append(TLS_program.phases[i].state[TLS_connID])
            if TLS_program.phases[i].state[TLS_connID] == 'r' or TLS_program.phases[i].state[TLS_connID] == 'y':
                TLS_redduration += TLS_program.phases[i].duration
            elif TLS_program.phases[i].state[TLS_connID] == 'G' or TLS_program.phases[i].state[TLS_connID] == 'g':
                TLS_greenduration += TLS_program.phases[i].duration
        TLS_phases_rotated = TLS_phases[TLS_program.currentPhaseIndex:] + TLS_phases[:TLS_program.currentPhaseIndex]
        TLS_index_rotated = list(range(len(TLS_phases)))[TLS_program.currentPhaseIndex:] + list(range(len(TLS_phases)))[:TLS_program.currentPhaseIndex]
        
        
        if TLS_state == 'G' or TLS_state == 'g':
            try:
                TLS_index_nextphase_rotated = TLS_phases_rotated.index('y')
            except ValueError:
                try:
                    TLS_index_nextphase_rotated = TLS_phases_rotated.index('r')
                except ValueError:
                    TLS_index_nextphase_rotated = []
                    
            for index in TLS_index_rotated[1:TLS_index_nextphase_rotated]:
                TLS_phaseremaindur += TLS_program.phases[index].duration
                
                
            TLS_timings = TLS_time_to_switch + TLS_phaseremaindur
            
            
        elif TLS_state == 'y' or TLS_state == 'r':
            try:
                TLS_index_nextphase_rotated_G = TLS_phases_rotated.index('G')
            except ValueError:
                TLS_index_nextphase_rotated_G = 1000
            try:
                TLS_index_nextphase_rotated_g = TLS_phases_rotated.index('g')
            except ValueError:
                TLS_index_nextphase_rotated_g = 1000
            TLS_index_nextphase_rotated = min(TLS_index_nextphase_rotated_G, TLS_index_nextphase_rotated_g)
            
            for index in TLS_index_rotated[1:TLS_index_nextphase_rotated]:
                TLS_phaseremaindur += TLS_program.phases[index].duration
                
            TLS_timings = TLS_time_to_switch + TLS_phaseremaindur
            
    else:
        TLS_timings = 100
        
    return TLS_timings
            
        
        
        
        