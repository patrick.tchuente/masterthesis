# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 17:38:08 2020

@author: Tchuente
"""

from pandas import read_csv, concat
from matplotlib import pyplot
from numpy import nan, isnan
import numpy as np
import pandas as pd
import math

from sklearn.metrics import mean_squared_error

from sklearn.metrics import mean_squared_error
from tensorflow.keras import backend as keras
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import LSTM, Dense, RepeatVector, Activation, Dropout, Bidirectional, TimeDistributed
from tensorflow. keras.utils import plot_model
import tensorflow
import matplotlib.pyplot as plt
from tensorflow.keras.optimizers import SGD
from sklearn.preprocessing import StandardScaler, MinMaxScaler


t_step = 10
"""Daten normalisieren (zwischen 0 und 1)"""
def nornalize_data(data):
    #normalize data
    data_min = data.min()
    data_max = data.max()
    
    # data = (data-data_min)/(data_max -data_min)
    return data, data_min, data_max



""" Definition of train and test shape"""
# split a multivariate dataset into train/test sets for Training and Test
def split_dataset(data):
    train_data, test_data = data[:train_split], data[train_split:]
    return train_data, test_data 


""" Data as Supervised Problem"""
#convert train data into inputs and aoutputs
def to_supervised(train_data, n_input, n_out = t_step):
    X, y = list(), list()
    in_start = 0
    # step over the entire history one time step at a time
    for _ in range(len(train_data)):
        #define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_out
        # ensure we have enough data for this instance
        if out_end <= len(train_data):
            X.append(train_data[in_start:in_end, :])
            y.append(train_data[in_end:out_end, index_leader_Speed])
        # move along one time step
        in_start += 1
        
    X_Train= np.array(X)
    Y_Train= np.array(y)
       
    return X_Train, Y_Train



#convert train data into inputs and aoutputs
def from_supervised(test_data, n_input, n_out = t_step):
    X, y = list(), list()
    in_start = 0
    # step over the entire history one time step at a time
    for _ in range(len(test_data)):
        #define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_out
        # ensure we have enough data for this instance
        if out_end <= len(test_data):
            X.append(test_data[in_start:in_end, :])
            y.append(test_data[in_end:out_end, index_leader_Speed])
        # move along one time step
        in_start += 1
        
    X_Test= np.array(X)
    Y_Test= np.array(y)
    
    
    return X_Test, Y_Test #, X_Test_tr, Y_Test_tr, x_test_scaler, y_test_scaler



""" Building the model"""
# train the model
def build_model( X_Train, Y_Train, n_input):
    # define parameters
    verbose, epochs, batch_size = 1, 60, 50
    n_timesteps, n_features, n_outputs = X_Train.shape[1], X_Train.shape[2], Y_Train.shape[1]
    # reshape output into [samples, timesteps, features]
    Y_Train = Y_Train.reshape((Y_Train.shape[0], Y_Train.shape[1], 1))
    #define model
    model = Sequential()
    model.add(LSTM(288, activation='relu', input_shape=(n_timesteps, n_features)))
    model.add(Dropout(0.2))
    model.add(RepeatVector(n_outputs))
    model.add(LSTM(288, activation='relu', return_sequences=True))
    model.add(TimeDistributed(Dense(288, activation='relu')))
    model.add(TimeDistributed(Dense(1)))
    model.compile(loss='mae', optimizer='adam', metrics=['acc', 'mae'])
    # fit network
    X_Val, Y_Val = X_Train[-val_split:], Y_Train[-val_split:]
    history = model.fit(X_Train[:-val_split], Y_Train[:-val_split], epochs=epochs, batch_size=batch_size, validation_data=(X_Val, Y_Val),  verbose=verbose)
    
    # Model Architectur
    print(model.summary())
    acc = history.history['acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1, len(acc) + 1)
    
    #plotting training and validation loss
    plt.plot(epochs, loss, label='Training loss')
    plt.plot(epochs, val_loss, label='validation loss')
    plt.title('Training and validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()
    
    #plotting training and validation accuracy
    plt.clf()

    val_acc = history.history['val_acc']
    plt.plot(epochs, acc, label='Training acc')
    plt.plot(epochs, val_acc, label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('acc')
    plt.legend()
    plt.show()
    
    model.save('LSTM_Model_Multistage08_8s_CT_4_24.h5')  # creates a HDF5 file 'my_model_.h5'
    return model, X_Val, Y_Val



    
data1 = read_csv('data_set1_08_2303_CT.csv')
data2 = read_csv('data_set2_08_2303_CT.csv')
data3 = read_csv('data_set3_08_2303_CT.csv')
data4 = read_csv('data_set4_08_2303_CT.csv')
data5 = read_csv('data_set5_08_2303_CT.csv')
data6 = read_csv('data_set6_08_2303_CT.csv')
data7 = read_csv('data_set7_08_2303_CT.csv')
data8 = read_csv('data_set8_08_2303_CT.csv')
data9 = read_csv('data_set9_08_2303_CT.csv')
data10 = read_csv('data_set10_08_2303_CT.csv')
data11 = read_csv('data_set11_08_2303_CT.csv')
data12 = read_csv('data_set12_08_2303_CT.csv')
    



data_all = [data1, data2, data3, data4, data5,
            data6, data7, data8, data9,
            data11, data12]

data_raw = pd.concat(data_all)
del data_raw['meanSpeedOnEdge']
del data_raw['traffic_density']
del data_raw['traffic_flow']


train_split = round(0.8*len(data_raw))
val_split = round(0.1*train_split)
test_split = round(0.2*len(data_raw))





#initialize

index_leader_Speed = 8
n_input = 10
split = 26000
t_step = 10

#scale data
data, data_min, data_max = nornalize_data(data_raw)
data = data.values


#split into train and test
train_data, test_data = split_dataset(data)

# # supervised (input an output)
X_Train, Y_Train = to_supervised(train_data, n_input, n_out = t_step)

# #test data
X_Test, Y_Test = from_supervised(test_data, n_input, n_out = t_step)


#the lstm model
#model, X_Val, Y_Val = build_model(X_Train, Y_Train, n_input)










