# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 14:02:47 2020

@author: tchuente
"""



import os
import sys

from collections import namedtuple
from myvehicles import myVehiles, Routes
#from data_collector import info_egoVeh, info_LEADER, infoSubcription
import pandas as pd
#from termcolor import colored
from TLS_Timing import time_state_switch
import tensorflow
import matplotlib.pyplot as plt
import numpy as np
from termcolor import colored




#SUMO_HOME = "C:\Program Files (x86)\Eclipse\Sumo"

# import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(
        os.path.dirname(__file__), "..", "..", "..")), "tools"))  # tutorial in docs
except ImportError:
    sys.exit(
        "please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")
    
      
import traci
import traci.constants as tc




def run():
    
    # create route Ego-vehicle
    route = traci.simulation.findRoute(my_route[0], my_route[1])
    #create Ego_vehicle with his route
    veh_ego = myVehiles(vehID, str("".join(('route_', vehID))), "myEgoCars", route.edges)
    #add route
    traci.route.add(veh_ego.RouteID, route.edges )
    #add vehicle
    traci.vehicle.add(veh_ego.ID, veh_ego.RouteID, typeID='myEgoCars')  # Ego vehicle
    traci.vehicle.setLaneChangeMode(veh_ego.ID, 257)
    
    #SUMO subscriptions
    traci.vehicle.subscribeLeader(veh_ego.ID, 10000)
    
    traci.vehicle.subscribe(veh_ego.ID, [tc.VAR_POSITION, tc.VAR_SPEED, tc.VAR_ACCELERATION , tc.VAR_ALLOWED_SPEED, tc.VAR_LANE_INDEX, tc.VAR_NEXT_TLS,
                                             tc.VAR_NEXT_STOPS, tc.VAR_ANGLE, tc.VAR_LANEPOSITION_LAT, tc.VAR_LANE_ID])
    
    traci.vehicle.subscribeContext(veh_ego.ID, tc.CMD_GET_VEHICLE_VARIABLE, 100.0, [tc.VAR_POSITION, tc.VAR_SPEED, tc.VAR_ACCELERATION, 
                                                                                    tc.VAR_ALLOWED_SPEED, tc.VAR_LANE_INDEX, tc.VAR_LANE_ID, 
                                                                                    tc.VAR_ANGLE, tc.VAR_LANEPOSITION_LAT])
    
    time_step = 0
    
    
    #solange das Ego_Fahrzeug noch unterwegs ist
    while vehID not in traci.simulation.getArrivedIDList():
        
        #simulationsschrit aktualisieren
        traci.simulationStep()
        
        if vehID not in traci.simulation.getArrivedIDList():

            #Ergebnisse der Abonementen
            resultsSub = traci.vehicle.getAllSubscriptionResults()
            resultContextSub = traci.vehicle.getContextSubscriptionResults(veh_ego.ID)
            
            
            
            print(vehID)
            print (time_step)
            
            features['ego_speed'].append(resultsSub[vehID][tc.VAR_SPEED])
            features['ego_acceleration'].append(resultsSub[vehID][tc.VAR_ACCELERATION])
            features['allowedSpeed'].append(resultsSub[vehID][tc.VAR_ALLOWED_SPEED])
            features["ego_distToTLS"].append(resultsSub[vehID][tc.VAR_NEXT_TLS][0][2] if len(resultsSub[vehID][tc.VAR_NEXT_TLS]) != 0 else 500)
            
            
            signalTLS =  resultsSub[vehID][tc.VAR_NEXT_TLS][0][3] if len(resultsSub[vehID][tc.VAR_NEXT_TLS]) != 0 else 'x'
            if signalTLS == 'G':
                features["signalTLS"].append(0)
            elif signalTLS == 'g':
                features["signalTLS"].append(1)
            elif signalTLS == 'r':
                features["signalTLS"].append(2)
            elif signalTLS == 'y':
                features["signalTLS"].append(3)
            elif signalTLS == 'x':
                features["signalTLS"].append(4)
            else:
                features["signalTLS"].append(5)
            

            result_TLS = resultsSub[vehID][tc.VAR_NEXT_TLS]
            current_simtime  = traci.simulation.getCurrentTime()/1000
            features['time_state_switch'].append(time_state_switch(result_TLS, current_simtime, traci))
            
            # falls das Ego_Vehicle kein Leader hat
            if resultsSub[vehID][tc.VAR_LEADER] is None:
                features['leader_speed'].append(resultsSub[vehID][tc.VAR_SPEED])
                features['leader_acceleration'].append(resultsSub[vehID][tc.VAR_ACCELERATION])
                features['distTEgoToLeader'].append(1000)
            else:
                features['leader_speed'].append(traci.vehicle.getSpeed(resultsSub[vehID][tc.VAR_LEADER][0]))
                features['leader_acceleration'].append(traci.vehicle.getAcceleration(resultsSub[vehID][tc.VAR_LEADER][0]))
                features['distTEgoToLeader'].append(resultsSub[vehID][tc.VAR_LEADER][1])
                
            speed, acceleration, numberLane, Id = list(), list(), list(), list()
            speedOfVehOnEgoLane, vehOnEgoLane= list(), list()
            
            this_road = True
            
            #Pro simulations step werden Eigenschaften aller Fahrzeuge sortiert
            for vehiculeID, subc in resultContextSub.items():
                #hier werden nur Fahrzeuge, die sich auf der Route des Egofahrzeugs befinden, berücksichtigt
                if traci.vehicle.getRoadID(vehiculeID) in traci.route.getEdges(traci.vehicle.getRouteID(vehID)):
                    
                    Id.append(vehiculeID)
                    speed.append(resultContextSub[vehiculeID][tc.VAR_SPEED])
                    acceleration.append(resultContextSub[vehiculeID][tc.VAR_ACCELERATION])
                    numberLane.append(traci.edge.getLaneNumber(traci.lane.getEdgeID(resultContextSub[vehiculeID][tc.VAR_LANE_ID ])))
                       
                    #Nur Fahrzeuge die sich auf der Spur des Egofahrzeug befinden.            
                    if resultContextSub[vehiculeID][tc.VAR_LANE_INDEX ] == resultsSub[veh_ego.ID][tc.VAR_LANE_INDEX]:
                        vehOnEgoLane.append(vehiculeID)
                        speedOfVehOnEgoLane.append(resultContextSub[vehiculeID][tc.VAR_SPEED])
                #Wenn gar keine Fahrzeuge auf der Route des Egofahr               
                elif veh_ego.ID not in Id:
                    try:       
                        speed.append(resultContextSub[vehID][tc.VAR_SPEED])
                        acceleration.append(resultContextSub[vehID][tc.VAR_ACCELERATION])
                        numberLane.append(traci.edge.getLaneNumber(traci.lane.getEdgeID(resultContextSub[vehID][tc.VAR_LANE_ID ])))
                        vehOnEgoLane.append(vehID)
                        speedOfVehOnEgoLane.append(resultContextSub[vehID][tc.VAR_SPEED])
                    except KeyError :
                        print(colored("Route hat Probleme, Egofahrzeug verschwunden. Fahrt beendet, neue Route wählen", 
                                      'green'))
                        this_road = False
            
                        
                    
            # Programm stoppen, wenn die Route ein Problem hat
            if this_road == False:
                break
            #mittlere Geschwindigkeit auf Ego-Spur 
            meanSpeedVehOnegoLane = sum(speedOfVehOnEgoLane)/len(speedOfVehOnEgoLane) if len(speedOfVehOnEgoLane) != 0 else  resultsSub[vehID][tc.VAR_SPEED]
            #Anzahl von Fahrspuren auf der aktuellen Edge
            numberOfLane = traci.edge.getLaneNumber(traci.vehicle.getRoadID(vehID))
            #Verkehrsfluss
            traffic_flow = len(Id)/simulationTimeStep
            #Mittlere Geschwindigkeit auf der Edge
            meanSpeedOnEdge = sum(speed)/len(speed)
            #Verkehrsdichte
            traffic_density = traffic_flow/meanSpeedOnEdge if meanSpeedOnEdge != 0 else 0
            
            
            features['numberLane'].append(numberOfLane)
            features['meanSpeedOnEdge'].append(meanSpeedOnEdge)
            features['meanSpeedOnEgoLane'].append(meanSpeedVehOnegoLane)
            features['traffic_flow'].append(traffic_flow)
            features['traffic_Density'].append(traffic_density)
            
    
            
        
            if time_step > sequense_size:
            
                dataset = pd.DataFrame(features)
                #nur die letzten Einträge betrachten
                data = dataset.tail(sequense_size)
               
                #Eleminieren der Parametter, die bei dem Modellaufbau nicht berücksichtigt wurden
                del data['meanSpeedOnEdge']
                del data['traffic_Density']
                del data['traffic_flow']
                
                #Nur die Werte holen
                input_data = data.values
                
                input_data = np.array(input_data)
                
                input_sequenz = input_data.reshape((1, input_data.shape[0], input_data.shape[1]))
                
                #vorbereitung input sequence für plot
                input_seq.append(input_sequenz)
                i_secquense = np.array(input_seq)
                i_secquense = i_secquense.reshape((i_secquense.shape[0], i_secquense.shape[2],i_secquense.shape[3]))
                
                
                #prädiktion
                output_sequence.append(model.predict(input_sequenz, verbose=1))
                
                #prädizierte sequenz
                sec_pred = np.array(output_sequence)
                sec_pred = sec_pred.reshape((sec_pred.shape[0], sec_pred.shape[2],1))
                
               
                #interactive mode on (online)
                #Es wird nur den 10. Schritt (Index = 9, beginnend bei 0) geplottet
                plt.ion()
                
                plt.show()
                plt.plot(sec_pred[:,9], linewidth= 1.5, label='leaderSpeed_pred')
                plt.plot(i_secquense[:,:,0][:,9], linewidth= 1, label='ego_speed', color ='r')
                plt.plot(i_secquense[:,:,8][:,9], linewidth= 1, label='Leader_speed', color ='g')
                plt.legend()
                
            time_step += 1
            
        else:
              break
              traci.close()      
        

    
            
    
    
if __name__ == "__main__":

    """Input ======================================================================================================="""
    my_sumo_config = "osm1DarmstadtCity.sumocfg"
    
    #my Ego-Vehicle
    vehID = "myEgoCar"

    
    #test Route
    # my_route = ["186513418", "405088379#2"]
    my_route = ["184877610", "150041579#0"]
    
    
    #collector
    features={'ego_speed':[],
             'ego_acceleration':[],
             'allowedSpeed':[],
             'distTEgoToLeader':[],
             'ego_distToTLS':[],
             'signalTLS':[],
             'meanSpeedOnEgoLane':[],
             'traffic_flow':[],
             'traffic_Density':[],
             'numberLane':[],
             'leader_speed':[],
             'leader_acceleration':[],
             'meanSpeedOnEdge':[],
             'time_state_switch':[],}
    
    input_seq, output_sequence = list(), list()
    
    
    
    
    #load model
    model = tensorflow.keras.models.load_model('LSTM_Model_Multistage08_8s_CT_1.h5')
    
    #model configuration
    config = model.get_config()
    
    #size input sequence
    sequense_size = config["layers"][0]["config"]["batch_input_shape"][1]


    simulationTimeStep = 0.6
    
    
    
                 


traci.start(["sumo-gui", "-c", my_sumo_config, '--no-warnings'])
run()