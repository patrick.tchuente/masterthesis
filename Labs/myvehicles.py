# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 14:08:03 2019

@author: Tchuente
"""

#definition of the class starts here  
class myVehiles:  
    #initializing the variables  
    ID = " "  
    RouteID = " "
    vType = " "
    Route = " "
        
    #defining constructor  
    def __init__(self, ID, RouteID, vType, Route):  
        self.ID = ID  
        self.RouteID = RouteID 
        self.vType = vType
        self.Route = Route
  
    #defining class methods  
    def showID(self):  
        print(self.ID)  
  
    def showRoute(self):  
        print(self.RouteID)
        
    def showvType(self):  
        print(self.vType)
        
class Routes:
    
    ID = " "
    edges = " "
    
    def __init__(self, ID, edges):  
      self.ID = ID  
      self.edges = edges