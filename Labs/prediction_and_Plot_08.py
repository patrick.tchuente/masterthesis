# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 13:30:19 2020

@author: tchuente
"""

import tensorflow
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error
import math
from LSTM_Model_MultyStage_08 import data_raw, X_Test, Y_Test

# Y_pred = np.load('Y_pred_08_2.npy')

#load model
model = tensorflow.keras.models.load_model('LSTM_Model_Multistage08_8s_CT_1.h5')


#preditions sequens2sequenc
predictions = list()
for i in range(X_Test.shape[0]):
    # predict the next 3 seconds
    output_sequence = model.predict(X_Test[i].reshape((1, X_Test.shape[1], X_Test.shape[2])), verbose=1)
    # store the predictions
    predictions.append(output_sequence[0])
Y_pred = np.array(predictions)


Y_pred = Y_pred.reshape((Y_pred.shape[0], Y_pred.shape[1]))

np.save('Y_pred_08_new',Y_pred )



""" ---------------------Plot der 10 Prädiktionschritte bei verschiedenen Zeitindexen--------------------"""

x0 = np.linspace(0, 10*0.8, num=10)

plt.subplot(221)
plt.plot(x0, Y_pred[173,:],label='predicted')
plt.plot(x0, Y_Test[173,:])
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 173')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(222)
plt.plot(x0, Y_pred[1640,:],label='predicted')
plt.plot(x0, Y_Test[1640,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1640')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(223)
plt.plot(x0, Y_pred[1647,:],label='predicted')
plt.plot(x0, Y_Test[1647,:],label='actual')
plt.xlabel('predictionstep(time in s)')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1647')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x0, Y_pred[1656,:],label='predicted')
plt.plot(x0, Y_Test[1656,:],label='actual')
plt.xlabel('predictionstep(time in s)')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1656')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()
"""_____________________________________________________________________"""


x00 = np.linspace(0, 10*0.8, num=10)

plt.subplot(221)
plt.plot(x00, Y_pred[1916,:],label='predicted')
plt.plot(x00, Y_Test[1916,:],label='actual')
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1916')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(222)
plt.plot(x00, Y_pred[4300,:],label='predicted')
plt.plot(x00, Y_Test[4300,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 4300')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(223)
plt.plot(x00, Y_pred[2450,:],label='predicted')
plt.plot(x00, Y_Test[2450,:],label='actual')
plt.xlabel('predictionstep(time in s)')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 2450')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x00, Y_pred[5916,:],label='predicted')
plt.plot(x00, Y_Test[5916,:],label='actual')
plt.xlabel('predictionstep(time in s)')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 5916')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()

"""_____________________________________________________________________________"""

plt.subplot(221)
plt.plot(x0, Y_pred[2530,:],label='predicted')
plt.plot(x0, Y_Test[2530,:],label='actual')
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 2530')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()



plt.subplot(222)
plt.plot(x0, Y_pred[1642,:],label='predicted')
plt.plot(x0, Y_Test[1642,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1642')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()


plt.subplot(223)
plt.plot(x0, Y_pred[1643,:],label='predicted')
plt.plot(x0, Y_Test[1643,:],label='actual')
plt.xlabel('predictionstep(time in s)')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1643')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x0, Y_pred[645,:],label='predicted')
plt.plot(x0, Y_Test[645,:],label='actual')
plt.xlabel('predictionstep(time in s)')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1645')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()

"""___________________________________________________________________________"""

x00 = np.linspace(0, 10*0.8, num=10)

plt.subplot(221)
plt.plot(x00, Y_pred[1646,:],label='predicted')
plt.plot(x00, Y_Test[1646,:],label='actual')
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1646')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(222)
plt.plot(x00, Y_pred[1647,:],label='predicted')
plt.plot(x00, Y_Test[1647,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1647')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(223)
plt.plot(x00, Y_pred[1648,:],label='predicted')
plt.plot(x00, Y_Test[1648,:],label='actual')
plt.xlabel('predictionstep(time in s)')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1238')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x00, Y_pred[1649,:],label='predicted')
plt.plot(x00, Y_Test[1649,:],label='actual')
plt.xlabel('predictionstep(time in s)')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1649')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()

"""________________________________________________________________________"""


x01 = np.linspace(0, 10*0.8, num=10)

plt.subplot(221)
plt.plot(x01, Y_pred[1650,:],label='predicted')
plt.plot(x01, Y_Test[1650,:],label='actual')
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1650')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(222)
plt.plot(x01, Y_pred[1651,:],label='predicted')
plt.plot(x01, Y_Test[1651,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1651')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(223)
plt.plot(x01, Y_pred[1652,:],label='predicted')
plt.plot(x01, Y_Test[1652,:],label='actual')
plt.xlabel('predictionstep(time in s)')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1652')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x01, Y_pred[1653,:],label='predicted')
plt.plot(x01, Y_Test[1653,:],label='actual')
plt.xlabel('predictionstep(time in s)')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1653')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()

"""_____________________________________________________________________"""
x01 = np.linspace(0, 10*0.8, num=10)

plt.subplot(221)
plt.plot(x01, Y_pred[1654,:],label='predicted')
plt.plot(x01, Y_Test[1654,:],label='actual')
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1654')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(222)
plt.plot(x01, Y_pred[1656,:],label='predicted')
plt.plot(x01, Y_Test[1656,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1656')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(223)
plt.plot(x01, Y_pred[1658,:],label='predicted')
plt.plot(x01, Y_Test[1658,:],label='actual')
plt.xlabel('predictionstep(time in s)')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1658')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x01, Y_pred[1660,:],label='predicted')
plt.plot(x01, Y_Test[1660,:],label='actual')
plt.xlabel('predictionstep(time in s)')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 1660')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()

"""____________________________________________________________________________"""
x0 = np.linspace(0, 10, num=10)

plt.subplot(221)
plt.plot(x0, Y_pred[4730,:],label='predicted')
plt.plot(x0, Y_Test[4730,:],label='actual')
# plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 4730')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(222)
plt.plot(x0, Y_pred[676,:],label='predicted')
plt.plot(x0, Y_Test[676,:],label='actual')
# plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 676')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(223)
plt.plot(x0, Y_pred[3387,:],label='predicted')
plt.plot(x0, Y_Test[3387,:],label='actual')
plt.xlabel('predictionstep')
plt.ylabel('speed (m/s)')
plt.title('Zeitindex 3387')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too
plt.legend()

plt.subplot(224)
plt.plot(x0, Y_pred[6754,:],label='predicted')
plt.plot(x0, Y_Test[6754,:],label='actual')
plt.xlabel('predictionstep')
# plt.ylabel('speed (m/s)')
plt.title('Zeitindex 6754')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.tight_layout()
plt.legend()
plt.show()


""" ----------Berechnung des MSE-------------"""


#evaluation
def evaluate_forecasts(Y_Test, Y_pred):

    # calculate an RMSE score for each step
    # calculate mse    
    mse = mean_squared_error(Y_Test, Y_pred, multioutput='raw_values')
    
    # calculate rmse
    rmse = list()
    for i in range(mse.shape[0]):
        s = math.sqrt(mse[i])
        rmse.append(s)
    
    #overall mse
    mseo = mean_squared_error(Y_Test, Y_pred)
    
    scores = rmse
    score = mseo
    mse_scores = mse

    return mse_scores, score, scores


def summarize_scores(name, score, scores):
    s_scores = ', '.join(['%.1f' % s for s in scores])
    print('%s: [%.3f] %s' % (name, score, s_scores))
    
    
    
mse_scores, score, scores = evaluate_forecasts(Y_Test, Y_pred)


""" ---------------------Plot des 10. Prädiktionschritts der Testdaten--------------------"""

Y_pred = Y_pred[10:][:,9]
Y_Test = Y_Test[:,9]

 
X1=np.linspace(0, 200, num=200)
X2=np.linspace(200, 400, num=200)
X3=np.linspace(400, 600, num=200)
X4=np.linspace(600, 800, num=200)


X5=np.linspace(800, 1000, num=200)
X6=np.linspace(1000, 1200, num=200)
X7=np.linspace(1200, 1400, num=200)
X8=np.linspace(1400, 1600, num=200)

X9=np.linspace(1600, 1800, num=200)
X10=np.linspace(1800, 2000, num=200)
X11=np.linspace(2000, 2200, num=200)
X12=np.linspace(2200, 2400, num=200)

X13=np.linspace(2400, 2600, num=200)
X14=np.linspace(2600, 2800, num=200)
X15=np.linspace(2800, 3000, num=200)
X16=np.linspace(3000, 3200, num=200)



plt.subplot(221)
plt.plot(X1, Y_pred[:200],label='predicted')
plt.plot(X1, Y_Test[:200],label='actual')
#plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too


plt.subplot(222)
plt.plot(X2, Y_pred[200:400], label='predicted')
plt.plot(X2, Y_Test[200:400], label='actual')
# plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(223)
plt.plot(X3, Y_pred[400:600], label='predicted')
plt.plot(X3, Y_Test[400:600], label='actual')
plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(224)
plt.plot(X4, Y_pred[600:800], label='predicted')
plt.plot(X4, Y_Test[600:800], label='actual')
plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too


plt.tight_layout()
plt.legend()
plt.show()


plt.subplot(221)
plt.plot(X5, Y_pred[800:1000], label='predicted')
plt.plot(X5, Y_Test[800:1000], label='actual')
# plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(222)
plt.plot(X6, Y_pred[1000:1200], label='predicted')
plt.plot(X6, Y_Test[1000:1200], label='actual')
# plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(223)
plt.plot(X7, Y_pred[1200:1400], label='predicted')
plt.plot(X7, Y_Test[1200:1400], label='actual')
plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(224)
plt.plot(X8, Y_pred[1400:1600],label='predicted')
plt.plot(X8, Y_Test[1400:1600],label='actual')
plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too




plt.tight_layout()
plt.legend()
plt.show()

plt.subplot(221)
plt.plot(X9, Y_pred[1600:1800],label='predicted')
plt.plot(X9, Y_Test[1600:1800],label='actual')
# plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too


plt.subplot(222)
plt.plot(X10, Y_pred[1800:2000], label='predicted')
plt.plot(X10, Y_Test[1800:2000], label='actual')
# plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(223)
plt.plot(X11, Y_pred[2000:2200], label='predicted')
plt.plot(X11, Y_Test[2000:2200], label='actual')
plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(224)
plt.plot(X12, Y_pred[2200:2400], label='predicted')
plt.plot(X12, Y_Test[2200:2400], label='actual')
plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too


plt.tight_layout()
plt.legend()
plt.show()


plt.subplot(221)
plt.plot(X13, Y_pred[2400:2600],label='predicted')
plt.plot(X13, Y_Test[2400:2600],label='actual')
# plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too


plt.subplot(222)
plt.plot(X14, Y_pred[2600:2800], label='predicted')
plt.plot(X14, Y_Test[2600:2800], label='actual')
# plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(223)
plt.plot(X15, Y_pred[2800:3000], label='predicted')
plt.plot(X15, Y_Test[2800:3000], label='actual')
plt.xlabel('timesteps')
plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too

plt.subplot(224)
plt.plot(X16, Y_pred[3000:3200], label='predicted')
plt.plot(X16, Y_Test[3000:3200], label='actual')
plt.xlabel('timesteps')
# plt.ylabel('speed (m/s)')
plt.grid(True)
plt.gca().xaxis.grid(True, which='minor')  # minor grid on too


plt.tight_layout()
plt.legend()
plt.show()



"""  Plot ders MSE """
summarize_scores('lstm_10', score, scores)

# plot scores
#sim_step = ['0.2s', '0.4s', '0.6s', '0.8s', '1s', '1.2s', '1.4s', '1.6s', '1,8', '2']
sim_step = ['0.8', '1.6', '2.4', '3.2', '4', '4.8', '5.6', '6.4', '7.2', '8']
plt.plot(sim_step, scores, marker='o', label='lstm')
plt.ylabel('MSE')
plt.grid(True)
plt.xlabel('Predictionsteps(s)')
plt.show()







#
#    testData = testData.reshape(-1)
#    
#    
#    plt.plot(predData[:500], label='LSp_red', color ='b')
#    plt.plot(testData[:500], label='LS_real', color ='r')
#    plt.plot(XtrainData[:,0][:500], color = 'g')
#    plt.legend()
#    plt.show()
#    
    
 
# Y_pred = Y_pred*(data_raw["leader_Speed"].max()-data_raw["leader_Speed"].min())+data_raw["leader_Speed"].min()
# Y_Test = Y_Test*(data_raw["leader_Speed"].max()-data_raw["leader_Speed"].min())+data_raw["leader_Speed"].min()

# Y_Test = Y_Test[:]
# Y_pred = Y_pred[:12]
#Y_pred = model.predict(X_Test, verbose=0)
# Y_pred = Y_pred.reshape((Y_pred.shape[0], Y_pred.shape[1]))

#
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
